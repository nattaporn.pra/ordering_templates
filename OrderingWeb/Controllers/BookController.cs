﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Text;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OrderingWeb.Data;
using OrderingWeb.Models;
using Microsoft.AspNetCore.Authorization;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;


namespace OrderingWeb.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookController : Controller
    {
        public readonly BookDBContext _context;
        private readonly IConfiguration _config;
        public BookController(BookDBContext context, IConfiguration config)
        {
            _context = context;
            _config = config;
        }

        [HttpGet("GetBooks")]
        public ActionResult<List<Book>> GetBook()
        {
            return _context.Books.ToList();
        }

        [HttpGet("SearchBook")]
        public ActionResult<List<Book>> SearchProduct([FromQuery] string search)
        {
            var result = _context.Books.Where(t => t.ProductId.ToString().Contains(search) ||
                    t.ProductName.Contains(search) || t.Price.ToString().Contains(search));
            var data = result.ToList();
            return data;

        }

        [HttpPost("CreateNewBook")]
        public async Task<ActionResult<Book>> CreateNewProduct([FromBody] Book prod)
        {
            await _context.Books.AddAsync(prod);
            await _context.SaveChangesAsync();
            return prod;
        }

        [HttpPost("UpdateBook/{id}")]
        public async Task<ActionResult<Book>> UpdateProduct(int id, [FromQuery] string productName, [FromQuery] decimal price)
        {
            Book result = null;
            bool isBadRequest = false;
            try
            {
                if (string.IsNullOrEmpty(productName))
                    throw new Exception("กรุณากรอกชื่อสินค้า");

                if (price < 1)
                    throw new Exception("ราคาสินค้าต้องมากกว่า 0");

                result = await _context.Books.Where(x => x.ProductId == id).FirstOrDefaultAsync();
                if (result == null)
                    throw new Exception($"ไม่พบสินค้ารหัส {id}");

                result.ProductName = productName;
                result.Price = price;

                _context.Update(result);
                await _context.SaveChangesAsync();

            }
            catch (Exception ex)
            {
                isBadRequest = true;
                result = new Book { ProductName = ex.Message };
            }

            if (isBadRequest)
                return BadRequest(result);

            return Ok(result);
        }

        [HttpDelete("DeleteBook/{id}")]
        public async Task<ActionResult<ResponseMessage>> DeleteProduct(int id)
        {
            try
            {
                var prod = await _context.Books.Where(x => x.ProductId == id).FirstOrDefaultAsync();
                if (prod == null)
                    return BadRequest(new ResponseMessage { statusCode = (int)System.Net.HttpStatusCode.BadRequest, message = $"ไม่พบรหัสสินค้า {id}" });

                _context.Remove(prod);
                await _context.SaveChangesAsync();

                return Ok(new ResponseMessage { statusCode = (int)System.Net.HttpStatusCode.OK, message = $"ลบข้อมูลสินค้ารหัส {id} เรียบร้อย" });
            }
            catch (Exception ex)
            {
                return StatusCode((int)System.Net.HttpStatusCode.InternalServerError, new ResponseMessage { statusCode = (int)System.Net.HttpStatusCode.InternalServerError, message = ex.Message });
            }
        }

        public class ResponseMessage
        {
            public int statusCode { get; set; }
            public string message { get; set; }

        }
    }
}
